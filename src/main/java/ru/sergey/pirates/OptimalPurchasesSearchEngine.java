package ru.sergey.pirates;

import java.util.Collection;

import pirates.Purchases;


public interface OptimalPurchasesSearchEngine {
    Purchases searchOptimalPurchases(Collection<RumProviderOffer> offers, int requiredGallons);
}
