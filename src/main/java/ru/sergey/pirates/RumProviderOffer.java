package ru.sergey.pirates;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode (of = "sourceName")
@ToString
@AllArgsConstructor
public class RumProviderOffer {
    /** Source Name. */
    private final String sourceName;

    /** Size. */
    private final int size;

    /** Average price of gallon. */
    private final double avgPrice;

    /** Min size. */
    private final int minSize;

    /** Step size. */
    private final int stepSize;
}
