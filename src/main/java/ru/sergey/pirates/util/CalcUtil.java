package ru.sergey.pirates.util;

import ru.sergey.pirates.RumProviderOffer;

public final class CalcUtil {
    private CalcUtil() { /* Hide constructor for utility class. */ }


    /**
     * Calculate Weighted Average Cost.
     */
    public static double calcWac(final int purchaseSize, final int totalSize, final double price) {
        if (purchaseSize > totalSize) {
            return purchaseSize * price / totalSize;
        } else {
            return ((double)purchaseSize / totalSize) * price;
        }
    }

    /**
     * Calculate max possible size.
     */
    public static int getMaxPossibleSize(final int requiredSize, final RumProviderOffer offer) {
        if (offer.getSize() <= requiredSize) {
            return offer.getSize();
        }

        if (offer.getMinSize() <= requiredSize) {
            return offer.getMinSize() + ((requiredSize - offer.getMinSize()) / offer.getStepSize()) * offer.getStepSize();
        } else {
            return offer.getMinSize();
        }
    }
}
