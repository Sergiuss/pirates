package ru.sergey.pirates.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.sergey.pirates.RumProviderOffer;

public final class RumProviderOfferReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(RumProviderOfferReader.class);

    private static final Charset UTF_8 = Charset.forName("UTF-8");
    private static final String CELL_DELIMITER = ";";

    private RumProviderOfferReader() { /* Hide constructor for utility class. */ }

    /**
     * Read offers from source file.
     * @param path to source file.
     * @return collection of {@link ru.sergey.pirates.RumProviderOffer}s
     * @throws IOException if file not found or has inappropriate format.
     */
    public static Collection<RumProviderOffer> readRumProviderOffers(final String path) throws IOException {
        final File sourceFile = new File(path);
        final Set<RumProviderOffer> rumProviderOffers = new HashSet<>();

        try (final BufferedReader sourceFileReader = new BufferedReader(new InputStreamReader(new FileInputStream(sourceFile), UTF_8))) {
            // skip first line (column headers)
            // may be check isHeader()?
            String sourceLine = sourceFileReader.readLine();
            LOGGER.debug("Column headers have been skipped: {}", sourceLine);

            if (sourceLine != null) {
                while ((sourceLine = sourceFileReader.readLine()) != null) {
                    rumProviderOffers.add(convert(sourceLine));
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            final StringBuilder stringBuilder = new StringBuilder();
            rumProviderOffers.forEach(offer -> stringBuilder.append(offer).append("\n"));
            LOGGER.debug("Source file contains next intelligence data: \n{}", stringBuilder);

        }

        return rumProviderOffers;
    }

    private static RumProviderOffer convert(final String sourceLine) {
        final String[] cells = sourceLine.split(CELL_DELIMITER);

        final String sourceName = cells[0].trim();
        final int size = Integer.parseInt(cells[1].trim());
        final double avgPrice = Double.parseDouble(cells[2].trim());
        final int minSize = Integer.parseInt(cells[3].trim());
        final int stepSize = Integer.parseInt(cells[4].trim());

        return new RumProviderOffer(sourceName, size, avgPrice, minSize, stepSize);
    }
}
