package ru.sergey.pirates.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.ToString;
import pirates.Purchase;
import pirates.Purchases;
import ru.sergey.pirates.JackSparrowHelperException;
import ru.sergey.pirates.OptimalPurchasesSearchEngine;
import ru.sergey.pirates.RumProviderOffer;
import ru.sergey.pirates.util.CalcUtil;

public class DefaultOptimalPurchasesSearchEngine implements OptimalPurchasesSearchEngine {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultOptimalPurchasesSearchEngine.class);

    private static final ShoppingList STUB_SHOPPING_LIST = new ShoppingList(new Purchase[0], 0, Double.NaN);

    @Override
    public Purchases searchOptimalPurchases(final Collection<RumProviderOffer> offers, final int requiredGallons) {
        final ShoppingList emptyShoppingList = new ShoppingList(new Purchase[0], requiredGallons, 0);

        final List<ShoppingList> shoppingLists = new ArrayList<>();
        shoppingLists.add(emptyShoppingList);

        ShoppingList optimalShoppingList = STUB_SHOPPING_LIST;

        int iterationCount = 0;

        for (final RumProviderOffer offer : offers) {
            final List<ShoppingList> newShoppingLists = new ArrayList<>();

             for (final ShoppingList shoppingList : shoppingLists) {
                iterationCount++;

                final ShoppingList newShoppingList = tryToFillShoppingList(shoppingList, offer, requiredGallons, newShoppingLists::add);

                if (Double.compare(optimalShoppingList.wac, newShoppingList.wac) > 0) {
                    optimalShoppingList = newShoppingList;
                }
            }

            shoppingLists.addAll(newShoppingLists);
        }

        LOGGER.info("Iteration count: {}", iterationCount);

        if (optimalShoppingList.isEmpty()) {
            throw new JackSparrowHelperException("There are no suitable providers");
        } else {
            final Set<Purchase> purchases = new HashSet<>();
            Collections.addAll(purchases, optimalShoppingList.purchases);

            final Purchases optimalPurchases = new Purchases(requiredGallons);
            optimalPurchases.setPurchases(purchases);

            return optimalPurchases;
        }
    }

    /**
     * Return fully filled shopping list or stub (non null).
     */
    private ShoppingList tryToFillShoppingList(final ShoppingList shoppingList, final RumProviderOffer offer, final int totalSize, final Consumer<ShoppingList> shoppingListRepository) {
        final int maxPossibleSize = CalcUtil.getMaxPossibleSize(shoppingList.requiredSize, offer);

        final double wac = CalcUtil.calcWac(maxPossibleSize, totalSize, offer.getAvgPrice());

        final ShoppingList newShoppingList = shoppingList.createNewShoppingList(createPurchase(offer, maxPossibleSize), wac);

        if (newShoppingList.requiredSize <= 0) {
            return newShoppingList;
        } else {
            shoppingListRepository.accept(newShoppingList);
        }

        return STUB_SHOPPING_LIST;
    }

    private Purchase createPurchase(final RumProviderOffer offer, final int size) {
        final Purchase purchase = new Purchase();
        purchase.setSourceName(offer.getSourceName());
        purchase.setPriceOfGallon(offer.getAvgPrice());
        purchase.setNumberOfGallons(size);
        return purchase;
    }

    @ToString
    private static class ShoppingList {
        private final Purchase[] purchases;
        private final int requiredSize;
        private final double wac;         // Weighted Average Cost

        public ShoppingList(final Purchase[] purchases, final int requiredSize, final double wac) {
            this.purchases = purchases;
            this.requiredSize = requiredSize;
            this.wac =  wac;
        }

        public boolean isEmpty() {
            return purchases.length == 0;
        }

        public ShoppingList createNewShoppingList(final Purchase purchase, final double wac) {
            final Purchase[] newPurchases = new Purchase[purchases.length + 1];
            System.arraycopy(purchases, 0, newPurchases, 0, purchases.length);
            newPurchases[purchases.length] = purchase;

            return new ShoppingList(newPurchases, requiredSize - purchase.getNumberOfGallons(), this.wac + wac);
        }
    }
}
