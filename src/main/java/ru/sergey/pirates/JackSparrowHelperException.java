package ru.sergey.pirates;


@SuppressWarnings("serial")
public class JackSparrowHelperException extends RuntimeException {

    public JackSparrowHelperException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public JackSparrowHelperException(final String message) {
        super(message);
    }
}
