package ru.sergey.pirates;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pirates.JackSparrowHelper;
import pirates.Purchases;
import ru.sergey.pirates.engine.DefaultOptimalPurchasesSearchEngine;
import ru.sergey.pirates.util.RumProviderOfferReader;

public class MyJackSparrowHelper implements JackSparrowHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyJackSparrowHelper.class);

    // TODO use DI
    private final OptimalPurchasesSearchEngine engine = new DefaultOptimalPurchasesSearchEngine();

    /**
     * {@inheritDoc}
     */
    @Override
    public Purchases helpJackSparrow(final String pathToPrices, final int numberOfGallons) {
        Collection<RumProviderOffer> offers = null;

        try {
            // TODO cache results
            offers = RumProviderOfferReader.readRumProviderOffers(pathToPrices);
        } catch (final Exception e) {
            LOGGER.error("Incorrect intelligence sources", e);

            throw new JackSparrowHelperException("Incorrect intelligence sources", e);
        }

        return engine.searchOptimalPurchases(offers, numberOfGallons);
    }
}
