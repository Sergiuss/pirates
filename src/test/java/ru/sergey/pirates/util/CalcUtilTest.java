package ru.sergey.pirates.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ru.sergey.pirates.RumProviderOffer;

public class CalcUtilTest {

    @Test
    public void calcWacPriceTest() {
        final int totalSize = 250;

        final int firstPurchaseSize = 50;
        final double firstPurchasePrice = 50.0;

        final int secondPurchaseSize = 200;
        final double secondPurchasePrice = 50.5;

        final double wac = CalcUtil.calcWac(firstPurchaseSize, totalSize, firstPurchasePrice)
                         + CalcUtil.calcWac(secondPurchaseSize, totalSize, secondPurchasePrice);

        assertEquals(50.4, wac, 0.009);
    }

    @Test
    public void calcMaxPossibleSizeTest() {
        final int size = 200;
        final int minSize = 50;
        final int stepSize = 50;

        final RumProviderOffer offer = new RumProviderOffer("test", size, 0, minSize, stepSize);

        // full size
        assertEquals(200, CalcUtil.getMaxPossibleSize(200, offer));
        assertEquals(200, CalcUtil.getMaxPossibleSize(250, offer));

        // min size
        assertEquals(50, CalcUtil.getMaxPossibleSize(50, offer));

        // minSize + stepSize
        assertEquals(100, CalcUtil.getMaxPossibleSize(100, offer));

        // minSize + 2*stepSize
        assertEquals(150, CalcUtil.getMaxPossibleSize(150, offer));

        // min size
        assertEquals(50, CalcUtil.getMaxPossibleSize(60, offer));

        // not suitable
        assertEquals(0, CalcUtil.getMaxPossibleSize(10, offer));
    }
}
