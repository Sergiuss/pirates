package ru.sergey.pirates.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import ru.sergey.pirates.BaseTestCase;
import ru.sergey.pirates.RumProviderOffer;

public class RumProviderOfferReaderTest extends BaseTestCase {
    private static final String EMPTY_SOURCES_FILE_NAME = "empty-sources.csv";

    @Test
    public void readSourceFileTest() throws IOException {
        runTest(SOURCES_FILE_NAME, 6);
    }

    @Test (expected = IOException.class)
    public void sourceFileNotFound() throws IOException {
        RumProviderOfferReader.readRumProviderOffers(SOURCES_FILE_NAME);
    }

    @Test (expected = NumberFormatException.class)
    public void incorrectSourceFile() throws IOException {
        runTest(INCORRECT_SOURCES_FILE_NAME, 0);
    }

    @Test
    public void emptySourceFile() throws IOException {
        runTest(EMPTY_SOURCES_FILE_NAME, 0);
    }

    private void runTest(final String fileName, final int expectedSources) throws IOException {
        final String path = getFilePath(fileName);

        final Collection<RumProviderOffer> rumProviderOffers =  RumProviderOfferReader.readRumProviderOffers(path);

        assertNotNull(rumProviderOffers);
        assertEquals(expectedSources, rumProviderOffers.size());
    }
}
