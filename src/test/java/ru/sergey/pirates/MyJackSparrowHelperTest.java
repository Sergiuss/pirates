package ru.sergey.pirates;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import pirates.JackSparrowHelper;
import pirates.Purchases;

public class MyJackSparrowHelperTest extends BaseTestCase {
    private final JackSparrowHelper helper = new MyJackSparrowHelper();

    @Test
    public void positiveVariationTest() {
        runTest(100, 51.0);

        runTest(150, 50.66);

        runTest(199, 50.75);

        runTest(200, 50.5);

        runTest(250, 50.4);

        runTest(300, 50.5);

        runTest(350, 50.42);

        runTest(400, 50.625);

        runTest(450, 50.55);

        runTest(500, 50.7 );
    }

    @Test(expected = JackSparrowHelperException.class)
    public void fewRumTest() {
        runTest(1000, 60);
    }

    @Test(expected = JackSparrowHelperException.class)
    public void incorrectSourceFileTest() {
        helper.helpJackSparrow(getFilePath(INCORRECT_SOURCES_FILE_NAME), 10);
    }

    private void runTest(final int numberOfGallons, final double expectedAvgPrice) {
        final Purchases purchases = helper.helpJackSparrow(getFilePath(SOURCES_FILE_NAME), numberOfGallons);

        LOGGER.info("Jack decided to buy {} gallons of Rum", numberOfGallons);
        LOGGER.info("Proposed shopping list:");
        purchases.getPurchases().forEach(purchase -> LOGGER.info("{}", purchase));
        LOGGER.info("Expected avg price: {}, calculated avg price: {}", expectedAvgPrice, purchases.calculateAveragePrice());
        LOGGER.info("=========================================================");

        assertEquals(expectedAvgPrice, purchases.calculateAveragePrice(), 0.009);
    }
}
