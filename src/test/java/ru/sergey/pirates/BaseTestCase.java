package ru.sergey.pirates;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseTestCase {
    protected static final Logger LOGGER = LoggerFactory.getLogger(BaseTestCase.class);
    protected static final String SOURCES_FILE_NAME = "sources.csv";
    protected static final String INCORRECT_SOURCES_FILE_NAME = "incorrect-sources.csv";

    protected String getFilePath(final String fileName) {
        return getClass().getResource(File.separator + fileName).getPath();
    }
}
